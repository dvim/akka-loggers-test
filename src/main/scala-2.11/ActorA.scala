import akka.actor._
import org.slf4j.LoggerFactory

/**
  * @author Aivaras Voveris <aivaras@eskimi.com>
  */
class ActorA extends Actor with ActorLogging {

  val log2 = LoggerFactory.getLogger("ActorA")

  def receive = {
    case _ =>
      log.warning("Got message")
      log2.warn("Got message")
  }

}
