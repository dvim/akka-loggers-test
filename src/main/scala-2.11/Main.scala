import akka.actor._

/**
  * @author Aivaras Voveris <aivaras@eskimi.com>
  */
object Main extends App  {

  implicit val system = ActorSystem("test-system")

  val actorA = system.actorOf(Props[ActorA], "actora")
  val actorB = system.actorOf(Props[ActorB], "actorb")

  actorA ! "Hi"
  actorB ! "Hi"

}
